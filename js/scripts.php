<script src="js/jquery-3.5.1.min.js" type="text/javascript"></script>
<script type="text/javascript">

let time_block_button = 700;

let slides=$(".slyde");
  $(".l_arrow").click(function(){
    for (var i = 0; i < slides.length; i++) {
      if ($(slides[i]).hasClass("slide_m"))
      {
        if(i==0)
        {
          $(slides[i]).addClass("slide_r");
          $(slides[i]).removeClass("slide_m");
          $(slides[i+1]).removeClass("slide_r");
          $(slides[i+1]).addClass("last");
          $(slides[slides.length-2]).addClass("slide_l");
          $(slides[slides.length-1]).removeClass("slide_l");
          $(slides[slides.length-1]).addClass("slide_m");
          break;
        }
        else if(i==slides.length-1)
        {
          $(slides[i]).addClass("slide_r");
          $(slides[i]).removeClass("slide_m");
          $(slides[0]).removeClass("slide_r");
          $(slides[0]).addClass("last");
          $(slides[i-2]).addClass("slide_l");
          $(slides[i-1]).removeClass("slide_l");
          $(slides[i-1]).addClass("slide_m");
          break;
        }
        else if(i==1)
        {
          $(slides[i]).addClass("slide_r");
          $(slides[i]).removeClass("slide_m");
          $(slides[i+1]).removeClass("slide_r");
          $(slides[i+1]).addClass("last");
          $(slides[slides.length-1]).addClass("slide_l");
          $(slides[i-1]).removeClass("slide_l");
          $(slides[i-1]).addClass("slide_m");
          break;
        }
        else
        {
          $(slides[i]).addClass("slide_r");
          $(slides[i]).removeClass("slide_m");
          $(slides[i+1]).removeClass("slide_r");
          $(slides[i+1]).addClass("last");
          $(slides[i-2]).addClass("slide_l");
          $(slides[i-1]).removeClass("slide_l");
          $(slides[i-1]).addClass("slide_m");
          break;
        }
      }
    }
    $(".last").css({
      "animation":"to_left_click4 var(--speed_slider)"
      });
    $(".last").removeClass("last");
    $(".slide_l").css({"animation":"to_left_click1 var(--speed_slider)"});
    $(".slide_m").css({"animation":"to_left_click2 var(--speed_slider)"});
    $(".slide_r").css({"animation":"to_left_click3 var(--speed_slider)"});
    $(this).attr("disabled", true);
    $(".r_arrow").attr("disabled", true);

    setTimeout(function(){
      $(".l_arrow").attr("disabled", false);
      $(".r_arrow").attr("disabled", false);
    },time_block_button);

  });
  $(".r_arrow").click(function(){

    for (var i = 0; i < slides.length; i++) {
      if ($(slides[i]).hasClass("slide_m"))
      {
        if(i==0)
        {
          $(slides[i]).addClass("slide_l");
          $(slides[i]).removeClass("slide_m");
          $(slides[i+1]).removeClass("slide_r");
          $(slides[i+2]).addClass("slide_r");
          $(slides[slides.length-1]).removeClass("slide_l");
          $(slides[slides.length-1]).addClass("last");
          $(slides[i+1]).addClass("slide_m");
          break;
        }
        else if(i==slides.length-1)
        {
          $(slides[i]).addClass("slide_l");
          $(slides[i]).removeClass("slide_m");
          $(slides[0]).removeClass("slide_r");
          $(slides[1]).addClass("slide_r");
          $(slides[i-1]).removeClass("slide_l");
          $(slides[i-1]).addClass("last");
          $(slides[0]).addClass("slide_m");
          break;
        }
        else if(i==slides.length-2)
        {
          $(slides[i]).addClass("slide_l");
          $(slides[i]).removeClass("slide_m");
          $(slides[i+1]).removeClass("slide_r");
          $(slides[0]).addClass("slide_r");
          $(slides[i-1]).removeClass("slide_l");
          $(slides[i-1]).addClass("last");
          $(slides[i+1]).addClass("slide_m");
          break;
        }
        else
        {
          $(slides[i]).addClass("slide_l");
          $(slides[i]).removeClass("slide_m");
          $(slides[i+1]).removeClass("slide_r");
          $(slides[i+2]).addClass("slide_r");
          $(slides[i-1]).removeClass("slide_l");
          $(slides[i-1]).addClass("last");
          $(slides[i+1]).addClass("slide_m");
          break;
        }
      }
    }

    $(".last").css({
      "animation":"to_right_click1 var(--speed_slider)"
      });
    $(".last").removeClass("last");
    $(".slide_l").css({"animation":"to_right_click2 var(--speed_slider)"});
    $(".slide_m").css({"animation":"to_right_click3 var(--speed_slider)"});
    $(".slide_r").css({"animation":"to_right_click4 var(--speed_slider)"});
    $(this).attr("disabled", true);
    $(".l_arrow").attr("disabled", true);

    setTimeout(function(){
      $(".r_arrow").attr("disabled", false);
      $(".l_arrow").attr("disabled", false);
    },time_block_button);
  });
</script>
